package br.dev.hygino.repositories;

import br.dev.hygino.entities.Product;
import br.dev.hygino.tests.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
class ProductRepositoryTest {

    private long existingId;
    private long nonExistingId;
    private long countTotalProducts;

    @BeforeEach
    void setUp() throws Exception {
        existingId = 1L;
        nonExistingId = 1000L;
        countTotalProducts = 25L;
    }

    @Autowired
    private ProductRepository repository;

    @Test
    @DisplayName("Deve deletar o produto quando o Id existir")
    public void deleteShouldDeleteObjectWhenIdExists() {

        repository.deleteById(existingId);

        Optional<Product> result = repository.findById(existingId);
        Assertions.assertFalse(result.isPresent());
    }

    @Test
    @DisplayName("Busca por id deve retornar o produto quando o Id existir")
    public void findBydIdShouldReturnProductWhenIdExists() {

        Optional<Product> result = repository.findById(existingId);

        Assertions.assertFalse(result.isEmpty());
        Assertions.assertEquals(result.get().getName(), "The Lord of the Rings");
    }

    @Test
    @DisplayName("Busca por id não deve retornar o produto quando o Id for Inválido")
    public void findBydIdShouldNotReturnProductWhenIdNotExists() {

        Optional<Product> result = repository.findById(nonExistingId);

        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    @DisplayName("O método save deve inserir incrementando id automaticamente quanto o Id for nulo")
    public void saveShouldPersistWithAutoincrementWhenIdIsNull() {
        Product product = Factory.createProduct();
        product.setId(null);

        product = repository.save(product);

        Assertions.assertNotNull(product.getId());
        Assertions.assertEquals(countTotalProducts + 1, product.getId());
    }
}