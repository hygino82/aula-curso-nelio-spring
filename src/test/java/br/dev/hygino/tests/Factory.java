package br.dev.hygino.tests;

import br.dev.hygino.dto.CategoryDTO;
import br.dev.hygino.dto.ProductDTO;
import br.dev.hygino.entities.Category;
import br.dev.hygino.entities.Product;

import java.time.Instant;

public class Factory {
    public static Product createProduct() {
        Product product = new Product(
                1L,
                "Air Fryer",
                "Fritadeira sem óleo",
                321.75,
                "https://itatiaia.vtexassets.com/arquivos/ids/159114-1200-auto?v=638186489365400000&width=1200&height=auto&aspect=true"
                , Instant.parse("2024-04-05T03:00:00Z")
        );

        product.getCategories().add(new Category(2L, "Electronics"));

        return product;
    }

    public static ProductDTO createProductDTO() {
        Product product = createProduct();
        return new ProductDTO(product, product.getCategories());
    }

    public static Category createCategory() {
        return new Category(2L, "Electronics");
    }

    public static CategoryDTO createCategoryDTO() {
        var category = new Category(2L, "Electronics");
        return new CategoryDTO(category);
    }
}
