package br.dev.hygino.dto;

public record CategoryInsertDTO(String name) {
}
