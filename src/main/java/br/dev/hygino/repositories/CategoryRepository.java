package br.dev.hygino.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.dev.hygino.entities.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {
	
	@Query("SELECT obj FROM Category obj WHERE id = :id")
	public Optional<Category> searchById(Long id);
}